# README #

1.) Download subliminal:

	> pip install subliminal

2.) Add the script to your PYTHONPATH (if does not exist, [create one](https://stackoverflow.com/questions/3701646/how-to-add-to-the-pythonpath-in-windows-7))

3.) Run the script from console (do not include .py if using -m flag):

	> python -m dsdownloader [options] [your video path]
	
or

	> python <your script path>/dsdownloader.py [options] [your video path]

### Quick summary ###

This is a simple script to download subtitles quickly in a directory.
Use -h option for help