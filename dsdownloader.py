import os
import argparse
from babelfish import *
from subliminal import *

### Functions

def isValidVideoFile(filename : str):
    if filename.lower().endswith(VIDEO_EXTENSIONS):
        # Do not download subtitle for samples
        if (not filename.lower().__contains__('.sample')):
            return True
    return False

### Main Script

# Argument parser

parser = argparse.ArgumentParser(
    description='Download subtitles in a directory',
    epilog='You can specify multiple languages, default is English. '
           'If you do not specify a path, the current directory will be used.')
parser.add_argument('--hun', action='store_true', help='Download Hungarian subtitles')
parser.add_argument('--eng', action='store_true', help='Download English subtitles')
parser.add_argument('-v', '--verbose', action='store_true', help='Verbose mode')
parser.add_argument('-s', '--silent', action='store_true', help='Silent mode')
parser.add_argument('rootDirectory', type=str, help='Path of the movie/series directory', nargs='?', default=os.getcwd())

args = parser.parse_args()

if args.verbose:
    print("Root search directory is " + args.rootDirectory)

# Configure the cache
#
# region.configure('dogpile.cache.dbm', arguments={'filename': 'cachefile.dbm'})

# Set languages

defaultLanguage = 'eng'
languages = set()

if (args.hun):
    languages.add(Language('hun'))

if (args.eng):
    languages.add(Language('eng'))

if (languages.__len__() <= 0):
    languages.add(Language(defaultLanguage))

# Collect the video information

videos = set()
videoDirs = dict()

if args.verbose:
    print("Searching for videos")

for subDir, dirNames, fileNames in os.walk(args.rootDirectory):
    if args.verbose:
        print("Searching in directory: " + subDir)
    if (fileNames.__len__() > 0):
        for fileName in fileNames:
            if (isValidVideoFile(fileName)):
                try:
                    video = Video.fromname(fileName)
                    if (isinstance(video.episode, list)):
                        if not args.silent:
                            print("Multiple episode in one file is not supported, skipping file: " + fileName)
                        continue
                    videos.add(video)
                    videoDirs[video.name] = subDir
                except ValueError:
                    print("Error happened while processing the file: " + os.path.join(subDir, fileName))
                    exit(1)

# Some checks

if len(videos) <= 0:
    if not args.silent:
        print("Did not find any videos")
    exit(0)
elif (len(videos) > 20):
    while True:
        user_input = input(len(videos).__str__() + " videos were found, are you sure you want to continue? (y/n): ")
        if user_input.lower() in ['y', 'n']:
            break
        else:
            print("Please provide a valid answer")

    if user_input.lower() == 'n':
        exit(0)

# Download the subs

if not args.silent:
    print("Started downloading subtitles for " + len(videos).__str__() + " video(s)")

try:
    best_subs = download_best_subtitles(videos, languages)
except:
    print("Unexpected error happened while trying to download subtitles")
    print("Unexpected error:",sys.exc_info()[0])
    exit(1)

# Save subtitles
# Also count videos which has at least one valid subtitle downloaded

validSubs = 0

for video in videos:
    hasValidSub = False
    for i in range (0, len(best_subs[video])):
        if (best_subs[video][i].content is not None):
            hasValidSub = True
        if (args.verbose):
            print("Saving " + best_subs[video][i].language.alpha3 + " subtitle for: " + video.name)
        save_subtitles(video, [best_subs[video][i]], False, videoDirs[video.name])
    if (hasValidSub):
        validSubs += 1

if not args.silent:
    if (validSubs > 0):
        print("Saved subtitles for " + validSubs.__str__() + "/" + len(videos).__str__() + " video(s)")
    else:
        print("Did not find any subtitles")
    print("Script finished")